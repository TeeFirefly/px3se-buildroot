#!/bin/bash

enable_adb=yes
enable_bluetooth=yes
enable_sdcard_udisk=yes
enable_io_tool=yes
enable_carplay=no

#enable build app
enable_camera=yes
enable_cvbsView=no
enable_gallery=yes
enable_music=yes
enable_video=yes
enable_settings=yes
enable_wifi_demo=no

#select codec chip
codec_use_ES8396=no
codec_use_px3se=yes

#replace sshd_config
sshd_config=yes

#D-bus system config
dbus_system_config=yes

#vsftpd virtual user config
vsftpd_virtual_user=yes

#add adb support
enable_adb=yes

#select display platform
PLATFORM_WAYLAND=no

# Download SDK #
The SDK is about 1.2 GiB. You can clone from Gitlab directly. (It may be fail due to network flow limit).

We have provided a 7z ball of the .git directory at here :

* [Firefly-PX3-SE_Buildroot_git_180421.7z](http://www.t-firefly.com/share/index/listpath/id/c683d3133dda4c12ed8313d4c4ec6a1f.html)

Please check the md5 checksum before proceeding:
```
#!shell
$ md5sum Firefly-PX3-SE_Buildroot_git_180421.7z
21b8c26d61731ce7bacc9000d39f33ed  Firefly-PX3-SE_Buildroot_git_180421.7z
```

If it is correct, uncompress it:
```
#!shell
mkdir -p ~/proj/Firefly-PX3-SE
cd ~/proj/Firefly-PX3-SE
7zr x Firefly-PX3-SE_Buildroot_git_180421.7z 
git reset --hard
git remote add gitlab git@gitlab.com:TeeFirefly/px3se-buildroot.git
```
From now on, you can pull updates from gitlab:
```
#!shell
git pull gitlab firefly-px3se:firefly-px3se
```

Please visit the following website for more details of our Firefly-PX3-SE development board. Thank you.

  http://www.t-firefly.com/doc/product/index/id/35.html

# 下载 Android SDK #

SDK 非常巨大（约 6 GiB），您可以直接从 Gitlab 下仓库源码（由于流量限制的原因可能会出错）。

我们提供了 .git 目录的打包，放到官方云盘上以供下载：

* [Firefly-PX3-SE_Buildroot_git_180421.7z](http://www.t-firefly.com/share/index/listpath/id/c683d3133dda4c12ed8313d4c4ec6a1f.html)

下载完成后先验证一下 MD5 码：
```
#!shell
$ md5sum Firefly-PX3-SE_Buildroot_git_180421.7z
21b8c26d61731ce7bacc9000d39f33ed  Firefly-PX3-SE_Buildroot_git_180421.7z
```
```

确认无误后，就可以解压：
```
#!shell
mkdir -p ~/proj/Firefly-PX3-SE
cd ~/proj/Firefly-PX3-SE
7zr x Firefly-PX3-SE_Buildroot_git_180421.7z 
git reset --hard
git remote add gitlab git@gitlab.com:TeeFirefly/px3se-buildroot.git
```

之后就可以从 gitlab 处获取更新的提交，保持同步:
```
#!shell
git pull gitlab firefly-px3se:firefly-px3se
```

想了解更多我们 Firefly-PX3-SE 开发板的详情，请浏览以下网址，谢谢！

  http://www.t-firefly.com/doc/product/index/id/35.html
